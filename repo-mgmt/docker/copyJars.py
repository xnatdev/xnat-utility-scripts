import argparse
import os
import re
import shutil

ignore_files =[
    "-javadoc.jar",
    "-beanJar",
    "-sources.jar"
]

def shouldIgnore(file):
    for jar_type in ignore_files:
        if file.endswith(jar_type):
            return True
    return False
    
def copyJar(jar_src, jar_dst):
    files=os.listdir(jar_src)
    print(files)
    target_size =0
    target={}
    for file in files:
        if shouldIgnore(file):
            continue
        if not (file.endswith(".jar") or file.endswith(".war")):
            continue
        size=os.stat(jar_src+file).st_size
        if size>target_size:
            target_size=size
            target = {
                "file":file,
                "size": size
            }
    print(target)
    target_file=target["file"]
    shutil.copy(jar_src+target_file, jar_dst+target_file)

parser = argparse.ArgumentParser(description="copy jars for Docker image")
group = parser.add_argument_group('Required')
group.add_argument('--jar_src', action="store", dest='jar_src', required=True, help='Build artifacts')
group.add_argument('--jar_dst', action="store", dest='jar_dst', required=True, help='Jar files packed into docker image')
inputArguments = parser.parse_args()

copyJar(inputArguments.jar_src, inputArguments.jar_dst)
