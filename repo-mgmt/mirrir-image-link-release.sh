#!/bin/bash
set -a
if [[ $TYPE == "RC" ]]; then
    CURRENT_VERSION=${NEW_VERSION_MAIN}-SNAPSHOT
    NEW_VERSION=$NEW_VERSION_DEVELOP
else
    CURRENT_VERSION=${NEW_VERSION_MAIN}-RC-SNAPSHOT
    NEW_VERSION=$NEW_VERSION_MAIN
fi
RC_BRANCH=releases/${NEW_VERSION_MAIN}-rc
CONFIG_JSON=$WORKSPACE/mirrir-image-link-plugin.json
cat > $CONFIG_JSON << EOF
[
    {
        "repo":"git@bitbucket.org:radiologics/mirrir_image_link_plugin",
        "alias":"mirrir_image_link_plugin",
        "cmd":"chmod +x ./gradlew && ./gradlew clean xnatPluginJar",
        "oldVersion":"$CURRENT_VERSION",
        "newVersion":"$NEW_VERSION",
        "xnatOldVersion":"$XNAT_VERSION_OLD",
        "xnatNewVersion":"$XNAT_VERSION_NEW"
    }
]
EOF

$WORKSPACE/repo-mgmt/jenkins.sh
