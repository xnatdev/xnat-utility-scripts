#!/usr/bin/python

import argparse
import json
import os
import traceback
import re
import glob
import sys, shutil
from subprocess import check_call, call, CalledProcessError

parser = argparse.ArgumentParser(description="csv parser")
group = parser.add_argument_group('Required')
group.add_argument('--config', action="store", dest='codeConfig', required=True, help='Code configuration')
group.add_argument('--sourceBranch', action="store", required=True, help='Branch to merge in')
group.add_argument('--destBranch', action="store", required=False, help='Destination branch')
group.add_argument('--newBranch', action="store", required=False, help='New branch for changes')
group.add_argument('--resume', action="store", required=False, help='Resume at')
group.add_argument('--noBuild', action="store_true")
group.add_argument('--noUpdate', action="store_true")
group.add_argument('--noPush', action="store_true")
group.add_argument('--noPublish', action="store_true")
group.add_argument('--conflictTheirs', action="store_true")
group.add_argument('--interactive', action="store_true")
args = parser.parse_args()

if args.destBranch and args.newBranch:
    print("ERROR: You provided destBranch and newBranch arguments. Use the former for merging and the latter for creating a new branch from sourceBranch with different dependency versions.")
    exit(1)

wrkDir = os.getcwd()
if args.conflictTheirs:
    conflictFile = "%s/git-conflict.txt" % wrkDir
    conflictToFile = "|tee -ai %s" % conflictFile
diffToFile = ""
if args.noPush:
    diffFile = "%s/git-diff.txt" % wrkDir
    diffToFile = "|tee -ai %s" % diffFile

def cfgParamIsTrue(cfg, key):
    return key in cfg and cfg[key] 

def tagRepo(cfg):
    ssVersion=cfg["releaseVersion"]+"-RC-SNAPSHOT"
    pullGitRepo(cfg, cfg["branch"])
    try:
        if isTaggedRelease():
            check_call("git tag %s %s " % (cfg["releaseVersion"], ssVersion), shell=True)
        else:
            check_call("git tag %s " % ssVersion, shell=True)

        if args.noPush:
            return
        check_call("git push origin --tags", shell=True)
    
    except Exception as e:
        print("\n\nERROR: Tag repo  %s: %s\n%s" % (cfg["alias"], str(e), traceback.format_exc()))    
        
def pullGitRepo(cfg, branch):
    if os.path.exists(cfg["alias"]):
        os.chdir(cfg["alias"])
        check_call("git fetch", shell=True)
        check_call("git checkout %s" % branch, shell=True)
        check_call("git pull origin %s"  % branch, shell=True)
        check_call("git reset --hard HEAD", shell=True)
    else:
        check_call("git clone %s --branch %s %s" % (cfg["repo"], branch, cfg["alias"]), shell=True)
        os.chdir(cfg["alias"])
    print("Pulled into %s" % os.getcwd(), flush=True)
        
def modifyRepoBranch(cfg, branch, pushToBranch):
    if pushToBranch == branch:
        print("pushToBranch is same as branch.\n", flush=True)                
        return
    if args.noUpdate and args.noPush:
        print("checkout pushToBranch.\n", flush=True)                
        check_call("git checkout %s" % pushToBranch, shell=True)
        return
    
    if args.destBranch:
        check_call("git checkout %s" % args.destBranch, shell=True)
        mergeMsg = "\"Merge branch '%s'\"" % branch
        if args.conflictTheirs:
            retcode = call("git merge %s -m %s" % (branch, mergeMsg), shell=True)
            if retcode != 0:
                check_call("git --no-pager diff %s" % conflictToFile, shell=True)
                check_call("git merge --abort", shell=True)
                check_call("git merge %s -X theirs -m %s" % (branch, mergeMsg), shell=True)
        else:
            check_call("git merge %s -m %s" % (branch, mergeMsg), shell=True)
    else:
        newBranch = determineNewBranch(cfg)
        if newBranch:
            check_call("git checkout -b %s" % newBranch, shell=True)
        check_call("echo %s && git log -1 && echo ''" % cfg["repo"], shell=True)
    
def updateParentLibVersion(cfg):
    # update lib version in parent
    if not cfgParamIsTrue(cfg, "separateVersion") or not versionChanged(cfg):
        return
    parentPath = os.path.join("..", "parent")
    parentPom = os.path.join(parentPath, "pom.xml")
    parentPomStr = loadFile(parentPom)
    pattern = re.compile(r"<nrg\.%s\.version>([^<]*)</nrg.%s\.version>" % (cfg["alias"], cfg["alias"]))
    parentPomStr = pattern.sub("<nrg.%s.version>%s</nrg.%s.version>" % (cfg["alias"], cfg['newVersion'], cfg["alias"]), parentPomStr)
    writeFile(parentPomStr, parentPom)
    check_call("cd %s && mvn clean install -DskipTests=true" % parentPath, shell=True)
    
def loadFile(fname):
    # Load whole file into mem bc we know it isn't large
    with open(fname, 'r') as file:
        content = file.read()
    return content

def replaceVersion(targetStr, cfg):
    if not versionChanged(cfg):
        return targetStr
    return targetStr.replace(cfg["oldVersion"], cfg["newVersion"])

def replaceXnatVersion(targetStr, cfg):
    if xnatVersionChanged(cfg):
        targetStr = targetStr.replace(cfg["xnatOldVersion"], cfg["xnatNewVersion"])
    return targetStr

def replaceDepVersions(targetStr, cfg):
    if "depVersions" in cfg:
        for v in cfg["depVersions"]:
            targetStr = targetStr.replace(v[0], v[1])
    return targetStr

def writeFile(content, fname):
    with open(fname, 'w') as file:
        file.write(content)

def updateChildPoms(cfg):
    for fname in glob.glob(os.path.join("*","pom.xml")):
        ts = loadFile(fname)
        ts = replaceVersion(ts, cfg)
        writeFile(ts, fname)

def versionChange(cfg):
    if args.noUpdate and args.noPush:
        print("Skipping version change.\n", flush=True)        
        return

    target = "build.gradle"
    if not os.path.isfile(target):
        target = "pom.xml"
        if not os.path.isfile(target):
            raise Exception("No build.gradle or pom.xml in %s" % cfg["alias"])
    targetStr = loadFile(target)

    # Do version change
    targetStr = replaceDepVersions(replaceXnatVersion(replaceVersion(targetStr, cfg), cfg), cfg)
    if isParent(cfg):
        pattern   = re.compile(r"<xnat\.version>[^<]*</xnat\.version>")
        targetStr = pattern.sub(r"<xnat.version>%s</xnat.version>" % cfg["newVersion"], targetStr)
        if isTaggedRelease():
            pattern   = re.compile(r"<xnatdev.version>[^<]*</xnatdev\.version>\s+")
            targetStr = pattern.sub("", targetStr)
            targetStr = targetStr.replace("${xnatdev.version}", "${xnat.version}")
            pattern   = re.compile(r"<xnatfix[^.]*.version>[^<]*</xnatfix[^.]*.version>\s+")
            targetStr = pattern.sub("", targetStr)
            targetStr = re.sub(r"\${xnatfix[^.]*\.version}", "${xnat.version}", targetStr)
    elif cfgParamIsTrue(cfg, "updateChildren"):
        updateChildPoms(cfg)

    # Write the file out again
    with open(target, 'w') as file:
        file.write(targetStr)        
    
    
def updateVersionAndBuildPushPublish(cfg, branch):
    versionChange(cfg)
    if hasChanges():
        writeDiffToFile()
    testBuild(cfg)
    commitAndPush(cfg, branch)
    if not isParent(cfg):
        publish(cfg)    
    publishToDocker(cfg)
    updateParentLibVersion(cfg)

def writeDiffToFile():
    check_call("git --no-pager diff %s" % diffToFile, shell=True)

def swapWebVersion(cfg, restore):
    if not isDataModels(cfg):
        return
    # to handle circular dep, build data models against prev web version
    target = "build.gradle"
    targetStr = loadFile(target)
    unversioned = 'implementation("org.nrg.xnat:web")'
    versioned = 'implementation("org.nrg.xnat:web:%s")' % cfg["oldVersion"]
    if restore:
        targetStr = targetStr.replace(versioned, unversioned)
    else:
        targetStr = targetStr.replace(unversioned, versioned)
    with open(target, 'w') as file:
        file.write(targetStr)

def testBuild(cfg):
    if args.noBuild and args.noPublish:
        print("Skipping local build and tests.\n", flush=True)
        return
    
    runCmd(cfg)

def publish(cfg):
    if args.noPublish or isPipelineEngine(cfg):
        print("Skipping publish.\n", flush=True)
        return

    runCmd(cfg, True)
    
def publishToDocker(cfg):
    if not (isWeb(cfg) or isPlugin(cfg)):
        return 
    if args.noPublish or isPipelineEngine(cfg):
        print("Skipping Docker image publish.\n", flush=True)
        return    

    copyJar =wrkDir+"/docker/copyJars.py "
    dockerShell = wrkDir+"/docker/docker-build.sh"
    image = "radiologicsit/"+cfg['alias']+":"+cfg['newVersion']
    srcJarFolder = os.getcwd()+"/build/libs/"    
    if isWeb(cfg): 
        targetFolder = wrkDir+"/docker/xnat-web-build/" 
    else:
        targetFolder = wrkDir+"/docker/xnat-plugin-build/"   
          
    dockerCommand = "chmod +x "+dockerShell+" &&"+ dockerShell+" -s "+srcJarFolder+" -d "+targetFolder + " -i "+image+ " -p "+copyJar   
    print(dockerCommand)
    
    check_call(dockerCommand, shell=True)
    
def runCmd(cfg, publish = False):
    swapWebVersion(cfg, False)
    cmd = cfg["cmd"]
    if publish:
        if "mvn" in cmd:
            cmd = "%s -DskipTests=true" % cmd.replace("install", "deploy")
        else:
            cmd = "%s -x test publish" % cmd.replace("unitTest", "") \
                .replace("test", "").replace("publishToMavenLocal", "")

    if args.interactive:
        retcode = call(cmd, shell=True)
        if retcode != 0:
            input(f"Please fix issues in {os.getcwd()} and hit Enter")
            runCmd(cfg, publish)
    else:
        check_call(cmd, shell=True)
    swapWebVersion(cfg, True)
    
def commitAndPush(cfg, branch, message = None, update = False):
    if args.noUpdate and args.noPush:
        print("Skipping commit and push.\n", flush=True)        
        return

    if not message:
        if versionChanged(cfg):
            message = "Update to version %s" % cfg["newVersion"]
        if xnatVersionChanged(cfg):
            if not message:
                message = "Update to"
            else:
                message += " and"
            message += " parent version %s"  % cfg["xnatNewVersion"]
                

    check_call("git commit -am '%s'" % message, shell=True)

    pushCmd = "git push origin %s" % branch
    if args.noPush:
        print("Skipping push: %s.\n" % pushCmd, flush=True)
        return

    check_call(pushCmd, shell=True)

    if isTaggedRelease() and versionChanged(cfg) and not isWeb(cfg):
        version = cfg["newVersion"]
        if update:
            check_call("git push origin --delete %s && git tag -f %s && git push origin %s" % (version, version, version), shell=True)
        else:
            check_call("git tag %s && git push origin %s" % (version, version), shell=True)

def isParent(cfg):
    return cfg["alias"] == "parent"

def isDataModels(cfg):
    return cfg["alias"] == "xnat-data-models"

def isWeb(cfg):
    return cfg["alias"] == "xnat-web"

def isContainerService(cfg):
    return cfg["alias"] == "container-service"

def isPipelineEngine(cfg):
    return cfg["alias"] == "xnat-pipeline-engine"

def isPlugin(cfg):
    if "isPlugin" in cfg and cfg["isPlugin"].upper() == "TRUE":
        return True
    return False
 
def hasChanges():
    try:
        check_call("git update-index --refresh; git diff-index --quiet HEAD --", shell=True)
        return False
    except CalledProcessError as e:
        if e.returncode == 1:
            return True
        else:
            raise

def isTaggedRelease():
    return args.destBranch == "master"

def versionChanged(cfg):
    return "oldVersion" in cfg and cfg["oldVersion"] != cfg["newVersion"]

def xnatVersionChanged(cfg):
    return "xnatOldVersion" in cfg and "xnatNewVersion" in cfg and cfg["xnatOldVersion"] != cfg["xnatNewVersion"]

def determineNewBranch(cfg):
    if not args.newBranch:
        return None
    return swapBranchSemVer(args.newBranch, cfg)

def swapBranchSemVer(branch, cfg):
    branchSemVer = parseSemVer(branch)
    if branchSemVer == branch:
        return branch
    newVersionSemVer = parseSemVer(cfg["newVersion"])
    return branch.replace(branchSemVer, newVersionSemVer)

def parseSemVer(value):
    return re.sub(r'[^0-9]*(([0-9]+\.)+[0-9]+)[^\.]*.*', r'\1', value)

def printFileToStdOut(message, file):
    if os.path.exists(file):
        print("%s printed to %s\n" % (message, file))
        with open(file, "rb") as f:
            shutil.copyfileobj(f, sys.stdout.buffer)

def getBranch():
    branch = args.sourceBranch
    if branch == "develop" and isContainerService(cfg):
        branch = "dev"
    branch = swapBranchSemVer(branch, cfg)
    return branch    

def getPushToBranch(srcBranch):
    pushToBranch = srcBranch
    if args.destBranch:
        pushToBranch = args.destBranch
    else:
        newBranch = determineNewBranch(cfg)
        if newBranch:
            pushToBranch = newBranch
    return pushToBranch

def updateOrPush():
    return not (args.noUpdate and args.noPush)        

wait = True
parentCfg = None
with open(args.codeConfig) as configFile:    
    cfgList = json.load(configFile)

for cfg in cfgList:
    if isParent(cfg):
        parentCfg = cfg
    if wait and args.resume is not None and args.resume != cfg["alias"]:
        continue
    elif args.resume is not None and args.resume == cfg["alias"]: 
        wait = False
    if  updateOrPush() and "tagRepo" in cfg:
        tagRepo(cfg)
        os.chdir(wrkDir)                
        continue    
    try:
        branch = getBranch()
        pullGitRepo(cfg, branch)
        pushToBranch = getPushToBranch(branch)
        if isParent(cfg):
            parentBranch = pushToBranch

        modifyRepoBranch(cfg, branch, pushToBranch)
        updateVersionAndBuildPushPublish(cfg, pushToBranch)
        
        # Update additional branches
        if "branch" in cfg and cfg["branch"] != args.sourceBranch:
            check_call("git checkout %s" % cfg["branch"], shell=True)
            updateVersionAndBuildPushPublish(cfg, cfg["branch"])

    except Exception as e:
        print("\n\nERROR: Issue updating %s: %s\n%s" % (cfg["alias"], str(e), traceback.format_exc()))
        exit(1)

    os.chdir(wrkDir)

if parentCfg:
    # Finally, if changes were made to parent due to repo version updates, push those
    os.chdir(parentCfg["alias"])
    if hasChanges():
        writeDiffToFile()
        commitAndPush(parentCfg, parentBranch, message = "Update versions for repos with separate versioning", update = True)
    publish(parentCfg)

if args.conflictTheirs and os.path.exists(conflictFile):
    printFileToStdOut("Git merge conflicts", conflictFile)

if diffToFile != "":
    printFileToStdOut("Git diff from dry run", diffFile)

