#!/bin/sh

# Task A: Dry run A
#    Pull code repositories (develop branch)
#    Change from version 1.8.4-SNAPSHOT to 1.8.5-SNAPSHOT
#    No build (-w)
#    No push  (-x)

source ./1.8.5-snapshot/common_develop.sh

REPO_LIST="$(pwd)/1.8.5-snapshot/repolist_a.json"

date
echo "Task A: Remove existing staging area: $FOLDER"
echo rm -rf $FOLDER
     rm -rf $FOLDER
echo ""

echo "Task A: Start the process of updating versions: $REPO_LIST"
echo ./update-versions.sh -w -x -d $FOLDER -p "$REPO_LIST" -v "$OLD_VERSION" -n "$NEW_VERSION" -b "$SOURCE_BRANCH"
     ./update-versions.sh -w -x -d $FOLDER -p "$REPO_LIST" -v "$OLD_VERSION" -n "$NEW_VERSION" -b "$SOURCE_BRANCH"

echo ""
echo "Code repositories updated."
echo "Task A: Complete"
date
