#!/usr/bin/python

import argparse
import json
from subprocess import check_call
import subprocess

parser = argparse.ArgumentParser(description="build code")
group = parser.add_argument_group('Required')
group.add_argument('--config', action="store", dest='codeConfig', required=True, help='code configuration')
group.add_argument('--staging', action="store", dest='staging', required=True, help='code staging directory')
group.add_argument('--build', action="store", dest='build', required=True, help='code build directory')
inputArguments = parser.parse_args()

with open(inputArguments.codeConfig) as config_file:    
    codeConfig = json.load(config_file)


check_call("echo 'ls -l "+ inputArguments.build + "';ls -l "+ inputArguments.build , shell=True)
print("LOG:Pull dependencies")
             
if 'dependencies' in codeConfig:
	for overlay in codeConfig["dependencies"]:
		check_call("echo 'ls -l "+ inputArguments.build + "';ls -l "+ inputArguments.build , shell=True)
		print("LOG:cp -r "+ inputArguments.staging + "/" + overlay["alias"] + " " + inputArguments.build + "/")
		check_call("cp -r "+ inputArguments.staging + "/" + overlay["alias"] + " " + inputArguments.build + "/", shell=True)
		
		check_call("echo 'ls -l "+ inputArguments.build + "';ls -l "+ inputArguments.build , shell=True)
		print("LOG:cd "+ inputArguments.build + "/" + overlay["alias"] +" && mvn clean install -DskipTests=true")
		if ( 'type' in overlay and overlay["type"] == "mvn" ):
			check_call("cd "+ inputArguments.build + "/" + overlay["alias"] +" && mvn clean install -DskipTests=true", shell=True)
		elif ( 'type' in overlay and overlay["type"] == "fatJar" ):
			check_call("cd "+ inputArguments.build + "/" + overlay["alias"] +" && chmod 755 ./gradlew && ./gradlew clean fatJar publishToMavenLocal -DskipTests=true", shell=True)
		else:
			check_call("cd "+ inputArguments.build + "/" + overlay["alias"] +" && chmod 755 ./gradlew && ./gradlew clean jar publishToMavenLocal -DskipTests=true", shell=True)

check_call("cp -r "+ inputArguments.staging + "/" + codeConfig["xnat-web"]["alias"] + " " + inputArguments.build + "/", shell=True)
check_call("echo BUILDING:"+ inputArguments.staging + "/" + codeConfig["xnat-web"]["alias"], shell=True)
web_cmd = "chmod 755 ./gradlew && ./gradlew clean build war publishToMavenLocal -x test"
if 'cmd' in codeConfig["xnat-web"]:
    web_cmd = codeConfig["xnat-web"]["cmd"]
check_call("cd "+ inputArguments.build + "/" + codeConfig["xnat-web"]["alias"] +" && " + web_cmd, shell=True)

if 'config' in codeConfig:
    check_call("cp -r " + inputArguments.staging + "/" + codeConfig["config"]["alias"] + " " + inputArguments.build + "/", shell=True) 
             
if 'plugins' in codeConfig:
	for overlay in codeConfig["plugins"]:
		check_call("cp -r "+ inputArguments.staging + "/" + overlay["alias"] + " " + inputArguments.build + "/", shell=True)
		check_call("echo BUILDING:"+ inputArguments.staging + "/" + overlay["alias"], shell=True)
		if ( 'cmd' in overlay):
			check_call("cd "+ inputArguments.build + "/" + overlay["alias"] +" && "+ overlay["cmd"], shell=True)
		elif ( 'type' in overlay and overlay["type"] == "mvn" ):
			check_call("cd "+ inputArguments.build + "/" + overlay["alias"] +" && mvn clean install -DskipTests=true", shell=True)
		elif ( 'type' in overlay and overlay["type"] == "fatJar" ):
			check_call("cd "+ inputArguments.build + "/" + overlay["alias"] +" && chmod 755 ./gradlew && ./gradlew clean fatJar publishToMavenLocal -x test", shell=True)
		else:
			check_call("cd "+ inputArguments.build + "/" + overlay["alias"] +" && chmod 755 ./gradlew && ./gradlew clean jar publishToMavenLocal -x test", shell=True)
