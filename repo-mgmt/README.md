# Cutting a release

See [jenkins.sh](jenkins.sh) for how we use this repo.

There are 3 types of "release":
*Snapshot* updates versions on develop branches, and isn't really a release at all. It uses the same logic, though, so it gets lumped in.
*RC* creates a new branch, `releases/<version>-rc` by convention, off tip of develop, and updates versions there
*Release* merges `releases/<version>-rc` onto master, and then updates versions on master

During a *snapshot* update, all repositories should have their XNAT dependency version incremented. If the repo itself has not changed and it doesn't share a version with parent/XNAT, you do not need to specify a new version for the repo.

During an *RC* or full *Release*, all repositories that share a version with parent/XNAT will also be released. If a repo doesn't share XNAT version and hasn't been changed, leave its oldVersion and newVersion equivalent and this will prevent its release.

## repolist.json 
The json repository list config file must contain a list of the repositories you wish to update in the format below. 

Required keys:
```
"repo":"ssh://git@bitbucket.org/xnatdev/test"   # repo 
"alias":"test"                                  # alias name for convenience, must be "parent" for parent
"cmd":"mvn clean install"                       # build command (include tests)
"oldVersion":"1.8.5-SNAPSHOT"                   # current version
"newVersion":"1.8.6-SNAPSHOT"                   # new version
```
Note that parent MUST have alias "parent"

Optional keys:
```
"separateVersion":true                          # if the repo tracks its own version and parent should be updated accordingly
"xdmTmp":true                                   # for the temporary build of data models needed to handle the circular dependency with web
"updateChildren":true                           # a few repos (ecat, dicom-xnat) have children that also need updating
"xnatOldVersion":"1.8.5-SNAPSHOT"               # current version of XNAT for repos with separate versioning
"xnatNewVersion:"1.8.6-SNAPSHOT"                # new version of XNAT for repos with separate versioning
"branch":"client"                               # additional branch to update
```

## Examples (may go out of date, jenkins.sh is the place to look)
```
# merge develop into master and update version for base repos
./update-versions.sh -w /opt/data -j $(pwd)/repolist.json -b develop -d master
# merge develop into master and update plugin dep version
./update-versions.sh -j -w /opt/data -j $(pwd)/plugins.json -b develop -d master
# update version on develop for base repos
./update-versions.sh -w /opt/data -j $(pwd)/repolist.json -b develop
# update plugin dep version on develop
./update-versions.sh -j -w /opt/data -j $(pwd)/plugins.json -b develop
# To create a new branch named "releases/1.8.0-rc" off of branch "develop"
./update-versions.sh -w /opt/data -j $(pwd)/repolist.json -b develop -n "releases/1.8.0-rc" 
```
