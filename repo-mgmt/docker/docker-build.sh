#!/bin/bash

set -e

usage() {
cat<<EOF
    USAGE: $0 -s [source Jar directory] -d [destination Jar directory] -i [docker image name] -p [copyJars.py Path]
EOF
}

echo "Setting options:"
while getopts "s:d:i:p:" opt; do
    case ${opt} in
        s)  echo source jar dir $OPTARG
            SRC_DIR=$OPTARG
            ;;
        d)  echo destination jar dir $OPTARG
            DST_DIR=$OPTARG
            ;;
        i)  echo Docker image name $OPTARG
            IMAGE=$OPTARG
            ;;
        p)  echo copyJars.py path $OPTARG
            COPYJAR_PATH=$OPTARG
            ;;
        \?) usage
            exit 1
            ;;
        *)  usage
            exit 1
            ;;

    esac
done

if [[ -z $docker_user ]]; then
    exit 0
fi

python3 $COPYJAR_PATH --jar_src $SRC_DIR --jar_dst $DST_DIR

cd $DST_DIR
JAR=$(find . -maxdepth 1 -type f \( -iname \*.jar -o -iname \*.war \))
echo $JAR
if [[ $JAR == *".war"* ]]; then
    TARGET=WEB-INF/classes/logback.xml 
else
    TARGET=$(jar xf ${JAR} $(jar tf ${JAR} | egrep 'META-INF/xnat/.*-plugin\.properties$'); egrep '^logConfiguration=' $(find META-INF/xnat -name "*-plugin.properties") | cut -f 2 -d =)
fi
if [[ -z $TARGET ]]; then
    echo "No log files"
else
    echo $TARGET
    rm -rf META-INF
    jar xf $JAR $TARGET
    python3 ../edit-log.py --logfile $TARGET
    jar uf ${JAR} ${TARGET}
fi

docker build $DST_DIR -t $IMAGE
docker login -u $docker_user -p $docker_password
docker push $IMAGE
docker image rm $IMAGE

