#!/bin/bash

set -e

export HOME=/var/lib/jenkins
export JAVA_HOME=/usr/lib/jvm/jre-1.8.0-openjdk
export PATH=$JAVA_HOME/bin:$PATH

# Remove cached repos
rm -rf $HOME/.m2/repository
rm -rf $HOME/.gradle/caches/modules-2
rm -rf $HOME/.cache/yarn/v6/.tmp

flags="-ulxp"
if [[ $UPDATE == true ]]; then
    flags=${flags//u}
fi
if [[ $PUSH == true ]]; then
    flags=${flags//x}
fi
if [[ $BUILD == true ]]; then
    flags=${flags//l}
fi
if [[ $PUBLISH == true ]]; then
    flags=${flags//p}
fi
if [[ $flags == "-" ]]; then
    flags=""
fi
echo $flags

# Define branches
developBranch=develop
mainBranch=master

# Cleanup working dirs
workDir=/opt/data/release
rm -rf $workDir
ssDir=/opt/data/release-ss
rm -rf $ssDir

# Define json repo list
cd $WORKSPACE/repo-mgmt

# Run scripts
if [[ $TYPE == "RC" ]]; then
    # update versions to RC
    rcJson=${CONFIG_JSON/.json}-rc.json
    python3 ./createRcJson.py --cfg $CONFIG_JSON --out $rcJson
    ./update-versions.sh $flags -w $workDir -j $rcJson -b $developBranch -n $RC_BRANCH

    # Update snapshot versions (skip publish since this autoruns on develop)
    ./update-versions.sh $flags -p -w $ssDir -j $CONFIG_JSON -b $developBranch
else
    # Merge RC into master 
    ./update-versions.sh $flags -c -w $workDir -j $CONFIG_JSON -b $RC_BRANCH -d $mainBranch
fi
