#!/bin/sh

for artifact in	\
	libs-release-local/org/nrg/parent/1.8.2		\
	libs-release-local/org/nrg/test/1.8.2		\
	libs-release-local/org/nrg/framework/1.8.2	\
	libs-release-local/org/nrg/transaction/1.8.2	\
	libs-release-local/org/nrg/prefs/1.8.2		\
	libs-release-local/org/nrg/config/1.8.2		\
	libs-release-local/org/nrg/automation/1.8.2	\
	libs-release-local/org/nrg/mail/1.8.2		\
	libs-release-local/org/nrg/notify/1.8.2		\
	libs-release-local/org/nrg/xdat/core/1.8.2	\
	libs-release-local/org/nrg/xnat/spawner/1.8.2	\
	libs-release-local/org/nrg/xdat/build/xdat-data-builder/1.8.2	\
	libs-release-local/org/nrg/xdat/build/xdat-data-builder/org.nrg.xdat.build.xdat-data-builder.gradle.plugin/1.8.2
do
 echo $artifact
 jfrog rt delete $artifact
done
