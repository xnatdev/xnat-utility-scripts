#!/bin/sh

source ./1.8.4-rc/common_develop.sh

REPO_LIST="$(pwd)/1.8.4-rc/repolist_a.json"

date
echo "Task A: Remove existing staging area: $FOLDER"
echo rm -rf $FOLDER
     rm -rf $FOLDER
echo ""

echo "Task A: Start the process of updating versions: $REPO_LIST"
echo ./update-versions.sh -d $FOLDER -p "$REPO_LIST" -b "$SOURCE_BRANCH" -z "$NEW_BRANCH"  -v "$OLD_VERSION" -n "$NEW_VERSION"
     ./update-versions.sh -d $FOLDER -p "$REPO_LIST" -b "$SOURCE_BRANCH" -z "$NEW_BRANCH"  -v "$OLD_VERSION" -n "$NEW_VERSION"

echo ""
echo "Code repositories updated."
echo "Task A: Complete"
date
