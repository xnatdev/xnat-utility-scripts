#!/bin/bash
set -a
if [[ $TYPE == "RC" ]]; then
    CURRENT_VERSION=${NEW_VERSION_MAIN}-SNAPSHOT
    NEW_VERSION=$NEW_VERSION_DEVELOP
else
    CURRENT_VERSION=${NEW_VERSION_MAIN/-INTERNAL}-RC-SNAPSHOT
    NEW_VERSION=$NEW_VERSION_MAIN
fi
RC_BRANCH=releases/${NEW_VERSION_MAIN/-INTERNAL}-rc
CONFIG_JSON=$WORKSPACE/xsync.json
cat > $CONFIG_JSON << EOF
[
    {
        "repo":"git@bitbucket.org:xnatdev/xsync",
        "alias":"xsync",
        "cmd":"chmod +x ./gradlew && ./gradlew clean fatJar",
        "isPlugin":"true",
        "oldVersion":"$CURRENT_VERSION",
        "newVersion":"$NEW_VERSION",
        "xnatOldVersion":"$XNAT_VERSION_OLD",
        "xnatNewVersion":"$XNAT_VERSION_NEW"
    }
]
EOF
OPEN_SOURCE=true
$WORKSPACE/repo-mgmt/jenkins.sh
