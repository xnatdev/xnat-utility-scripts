#!/usr/bin/python

import argparse
import json
import os
from subprocess import check_call, check_output

def checkRepoOrigin(directory,newOrigin):
    print("cd "+ directory +" && git config --get remote.origin.url")
    oldOrigin = check_call("cd "+ directory +" && git config --get remote.origin.url", shell=True)
    print("'{}'".format(oldOrigin))
    oldOrigin = oldOrigin.split('\n', 1)[0]
    print("'"+oldOrigin+"'")
    print(newOrigin)
    print(newOrigin == oldOrigin)
    return (newOrigin == oldOrigin)

def pullGitRepo(opt):
    ##Code to check if this is pulled from a different repo
    ##if os.path.exists(opt["alias"]):
    ##    check_call("cd "+ opt["alias"] +" && pwd", shell=True)
    ##    print(checkRepoOrigin(opt["alias"],opt["repo"]))

    ##exit(1)
    ##if os.path.exists(opt["alias"]) and checkRepoOrigin(opt["alias"],opt["repo"]):
    ##    check_call("echo rm -r "+ opt["alias"], shell=True)

    if os.path.exists(opt["alias"]):
        check_call("cd "+ opt["alias"] +" && git fetch", shell=True)
        check_call("cd "+ opt["alias"] +" && git checkout " + opt["version"], shell=True)
        check_call("cd "+ opt["alias"] +" && git pull origin " + opt["version"], shell=True)
    else:
        check_call("git clone "+opt["repo"] +" --branch " + opt["version"] +" " + opt["alias"], shell=True)

    check_call("cd %s && echo %s && git log -1 && echo -e '\n'" % (opt["alias"], opt["repo"]), shell=True)
    VERSIONS[opt["alias"]] = check_output("cd %s && git log --format='%%H' -n 1 | tr -d '\n'" % opt["alias"], shell=True)
    
        
parser = argparse.ArgumentParser(description="csv parser")
group = parser.add_argument_group('Required')
group.add_argument('--config', action="store", dest='codeConfig', required=True, help='Code configuration')
group.add_argument('--verFile', action="store", dest='verFile', required=True, help='File to store git commit ids')
group.add_argument('--pipeline', action="store_true", help='Pull pipeline code')
inputArguments = parser.parse_args()

if os.path.isfile(inputArguments.verFile):
    with open(inputArguments.verFile, 'r') as ci:
        VERSIONS = json.load(ci)
else:
    VERSIONS = {}

with open(inputArguments.codeConfig) as config_file:    
    codeConfig = json.load(config_file)

if inputArguments.pipeline:
    pullGitRepo(codeConfig["pipeline_builder"])

    if 'pipeline_overlays' in codeConfig:
        for pipelineOverlay in codeConfig["pipeline_overlays"]:
            pullGitRepo(pipelineOverlay)
    
    if 'pipeline_modules' in codeConfig:
        for module in codeConfig["pipeline_modules"]:
            pullGitRepo(module)

else:
    if 'dependencies' in codeConfig:
        for overlay in codeConfig["dependencies"]:
            pullGitRepo(overlay)
        
    pullGitRepo(codeConfig["xnat-web"])
          
    if 'plugins' in codeConfig:
        for overlay in codeConfig["plugins"]:
            pullGitRepo(overlay)
    
    if 'config' in codeConfig:
        pullGitRepo(codeConfig["config"])

with open(inputArguments.verFile, 'wb') as ci:
    ci.write(json.dumps(VERSIONS, sort_keys=True, indent=4, separators=(',', ': ')))

exit()
