#!/bin/bash
set -a
set -e

if [[ -z $XNAT_VERSION_MAIN || -z $XNAT_VERSION_DEVELOP ]]; then
    echo "XNAT_VERSION_MAIN and XNAT_VERSION_DEVELOP must be defined"
    exit 1
fi

repolist=$WORKSPACE/repo-mgmt/repolist.json
CONFIG_JSON=$WORKSPACE/${XNAT_VERSION_MAIN}-repolist.json
if [[ $TYPE == "RC" ]]; then
    sed -e "s/XNAT_VERSION_OLD/${XNAT_VERSION_MAIN}-SNAPSHOT/g" \
        -e "s/XNAT_VERSION_NEW/$XNAT_VERSION_DEVELOP/g" \
        -e "s/DE6_VERSION_OLD/${DE6_VERSION_MAIN}-SNAPSHOT/g" \
        -e "s/DE6_VERSION_NEW/$DE6_VERSION_DEVELOP/g" $repolist > $CONFIG_JSON
else
    sed -e "s/XNAT_VERSION_OLD/${XNAT_VERSION_MAIN/-INTERNAL}-RC-SNAPSHOT/g" \
        -e "s/XNAT_VERSION_NEW/$XNAT_VERSION_MAIN/g" \
        -e "s/DE6_VERSION_OLD/${DE6_VERSION_MAIN/-INTERNAL}-RC-SNAPSHOT/g" \
        -e "s/DE6_VERSION_NEW/$DE6_VERSION_MAIN/g" $repolist > $CONFIG_JSON
fi

RC_BRANCH=releases/${XNAT_VERSION_MAIN/-INTERNAL}-rc
$WORKSPACE/repo-mgmt/jenkins.sh
