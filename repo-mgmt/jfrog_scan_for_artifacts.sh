#!/bin/sh

# Inputs
#  Version (e.g. 1.8.2)

for artifact in	\
	parent test framework transaction prefs conf \
	automation mail notify xdat/core spawner xnat-data-models web xdat/build/xdat-data-builder ; do
 echo $artifact
 jfrog rt search "*$artifact*$1*" | jq '.[] | .path' | grep -v $1-SNAPSHOT | grep pom
done

#jfrog rt search "*xnat-web*1.7.7-SNAPSHOT*" | jq '.[] | .path'
