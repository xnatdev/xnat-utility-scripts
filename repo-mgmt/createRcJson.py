#!/usr/bin/python

import argparse
import json

parser = argparse.ArgumentParser(description="csv parser")
group = parser.add_argument_group('Required')
group.add_argument('--cfg', action="store", required=True, help='Repo json')
group.add_argument('--out', action="store", required=True, help='Output json')
args = parser.parse_args()

def updateNewVersionToRc(cfg, old, new):
    if cfg[new] != cfg[old]:
        cfg[new] = cfg[old].replace("SNAPSHOT", "RC-SNAPSHOT")

with open(args.cfg) as configFile:
    cfgList = json.load(configFile)

outList = []
for cfg in cfgList:
    if "oldVersion" in cfg:
        updateNewVersionToRc(cfg, "oldVersion", "newVersion")
    if "xnatOldVersion" in cfg:
        updateNewVersionToRc(cfg, "xnatOldVersion", "xnatNewVersion")
    outList.append(cfg)

with open(args.out, 'w') as f:
    json.dump(outList, f, indent=4)
