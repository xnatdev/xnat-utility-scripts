#!/bin/sh

source ./1.8.4-rc/common_develop.sh

REPO_LIST="$(pwd)/1.8.4-rc/repolist_d.json"

date
if [ ! -e $FOLDER ] ; then
 echo "Task D: This folder should already exist: $FOLDER"
 echo " It should have been created by the task_a.sh script"
 exit 1
fi

echo "Task D: Start the process of updating versions: $REPO_LIST"
echo ./update-versions.sh -d $FOLDER -p "$REPO_LIST" -b "$SOURCE_BRANCH" -z "$NEW_BRANCH"  -v "$OLD_VERSION" -n "$NEW_VERSION"
     ./update-versions.sh -d $FOLDER -p "$REPO_LIST" -b "$SOURCE_BRANCH" -z "$NEW_BRANCH"  -v "$OLD_VERSION" -n "$NEW_VERSION"

echo ""
echo "Code repositories updated."
echo "Task D: Complete"
date
