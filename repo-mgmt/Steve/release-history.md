# History of releases and commands executed

## 1.8.1

### Create RC branch from develop
```
./update-versions.sh -d /opt/data -p $(pwd)/repolist.json -b develop -z "releases/1.8.1-rc"  -v 1.8.1-SNAPSHOT -n 1.8.1-RC-SNAPSHOT
+ Updated dicomedit-pixels/develop branch to 1.8.1-SNAPSHOT
+ Moved pipeline-xnattools before plexiviewer
+ Build xnat-data-models by modifying parent/pom.xml to point at prior version of XNAT web
+ data-client does not build on ci.xnat.org. It has some weird publication stuff that needs to be resolved.
```
### Update develop branch to 1.8.2-SNAPSHOT
```
./update-versions.sh -d /opt/data -p $(pwd)/repolist.json -b develop -v 1.8.1-SNAPSHOT -n 1.8.2-SNAPSHOT
```

### After testing, update master to 1.8.1
```
./update-versions.sh -d /opt/data -p $(pwd)/repolist.json             -b releases/1.8.1-rc -r master -v 1.8.1-RC-SNAPSHOT -n 1.8.1

+ Did not do xnat-tasks the first time, so I did this separately
./update-versions.sh -d /opt/data -p $(pwd)/repolist-tasks.json       -b releases/1.8.1-rc -r master -v 1.8.1-RC-SNAPSHOT -n 1.8.1

+ Also had to data data-client separately
./update-versions.sh -d /opt/data -p $(pwd)/repolist-data-client.json -b releases/1.8.1-rc -r master -v 1.8.1-RC-SNAPSHOT -n 1.8.1

```

## 1.8.2

### Create RC branch from develop
```
./update-versions.sh -d /opt/data -p $(pwd)/repolist.json -b develop -z "releases/apr2021-rc"  -v 1.8.2-SNAPSHOT -n APR2021-RC-SNAPSHOT
+ Commit to bitbucket after creating branch failed for plexiviewer; assume bitbucket issue
++ Commit by hand worked
./update-versions.sh -d /opt/data -p $(pwd)/repolist.json -b develop -z "releases/apr2021-rc"  -v 1.8.2-SNAPSHOT -n APR2021-RC-SNAPSHOT -e pipeline-client
+ Creating a branch of xnat-data-models a second time failed (as you would expect)
./update-versions.sh -d /opt/data -p $(pwd)/repolist.json -b develop -z "releases/apr2021-rc"  -v 1.8.2-SNAPSHOT -n APR2021-RC-SNAPSHOT -e xnat-pipeline-engine
+ This command ran to completion

On the CI system, in the folder: /var/lib/jenkins/jobs/XNAT_Release_Candidate/jobs
 find . -name config.xml -exec sed -i -e 's/1.8.1-rc/apr2021-rc/' {} \;
 stop/restart Jenkins
 kick off the Release Candidate build by starting Parent
```

### Update develop branch to 1.8.3-SNAPSHOT
```
./update-versions.sh -d /opt/data -p $(pwd)/repolist.json -b develop -v 1.8.2-SNAPSHOT -n 1.8.3-SNAPSHOT
```

### After testing, update master to 1.8.2
```
./update-versions.sh -d /opt/data -p $(pwd)/repolist.json             -b releases/apr2021-rc -r master -v APR2021-RC-SNAPSHOT -n 1.8.2
./update-versions.sh -d /opt/data -p $(pwd)/repolist.json  -b releases/apr2021-rc -r master -v APR2021-RC-SNAPSHOT -n 1.8.2 -e prefs
```
The jobs in XNAT_Release_Build all point to the master branch of their respective repositories.
There is no need to update those when building a release version.

Had an issue building extattr on ci and had to delete a number of released jars (starting with parent) and start over.
See jfrog_delete_artifacts.sh and jfrog_scan_for_artifacts.sh

Data Models fails to build because of the circular dependency. On the CI server
```
cp /home/wustl/nrg-svc-jenkins/.m2/repository/org/nrg/xnat/web/APR2021-RC-SNAPSHOT/web-APR2021-RC-SNAPSHOT.jar /tmp/web-1.8.2.jar
~/apps/apache-maven-3.6.3/bin/mvn install:install-file -DgroupId=org.nrg.xnat -DartifactId=web -Dpackaging=jar -Dversion=1.8.2  -Dfile=//tmp/web-1.8.2.jar
cd /var/lib/jenkins/jobs/XNAT_Release_Build/jobs/280_Data_Models/workspace
git pull --all
./gradlew clean jar publishMavenJavaPublicationToMavenLocal
```

Start CI at 305_DICOM_Session_Builders

## 1.8.3

### Create RC branch from develop
```
This first run is for (parent, test, framework, transaction, preferences) while configuration is still in progress
./update-versions.sh -d /opt/data/1.8.3-RC -p /tmp/rc/repolist.json -b develop -z "releases/1.8.3-rc"  -v 1.8.3-SNAPSHOT -n 1.8.3-RC-SNAPSHOT

On the CI system, in the folder: /var/lib/jenkins/jobs/XNAT_Release_Candidate/jobs
 find . -name config.xml -exec sed -i -e 's/apr2021-rc/1.8.3-rc/' {} \;
 stop/restart Jenkins
 disable 125_Configuration
 kick off the Release Candidate build by starting 100_Parent

./update-versions.sh -d /opt/data/1.8.3-RC -p $(pwd)/repolist.json -b develop -z "releases/1.8.3-rc"  -v 1.8.3-SNAPSHOT -n 1.8.3-RC-SNAPSHOT
./update-versions.sh -d /opt/data/1.8.3-RC -p $(pwd)/repolist.json -b develop -z "releases/1.8.3-rc"  -v 1.8.3-SNAPSHOT -n 1.8.3-RC-SNAPSHOT -e config
./update-versions.sh -d /opt/data/1.8.3-RC -p $(pwd)/repolist.json -b develop -z "releases/1.8.3-rc"  -v 1.8.3-SNAPSHOT -n 1.8.3-RC-SNAPSHOT -e xdat-data-builder
./update-versions.sh -d /opt/data/1.8.3-RC -p $(pwd)/repolist.json -b develop -z "releases/1.8.3-rc"  -v 1.8.3-SNAPSHOT -n 1.8.3-RC-SNAPSHOT -e dicom-edit6
```
## 1.8.4-SNAPSHOT
./update-versions.sh -d /opt/data/1.8.4-SNAPSHOT -p $(pwd)/repolist.json -b develop -v 1.8.3-SNAPSHOT -n 1.8.4-SNAPSHOT

## General Notes
### Deleting a tag because we find we need another change after a repository was tagged:
https://support.atlassian.com/bitbucket-cloud/docs/repository-tags

### Using a prior version of web.jar so we can build xnat-data-models
These next steps are on the CI system.
```
cp /home/wustl/nrg-svc-jenkins/.m2/repository/org/nrg/xnat/web/1.8.3-SNAPSHOT/web-1.8.3-SNAPSHOT.jar /tmp/web-1.8.3-RC-SNAPSHOT.jar
~/apps/apache-maven-3.6.3/bin/mvn install:install-file -DgroupId=org.nrg.xnat -DartifactId=web -Dpackaging=jar -Dversion=1.8.3-RC-SNAPSHOT  -Dfile=/tmp/web-1.8.3-RC-SNAPSHOT.jar
cd /var/lib/jenkins/jobs/XNAT_Release_Build/jobs/280_Data_Models/workspace
./gradlew clean jar publishMavenJavaPublicationToMavenLocal
```

This version is for building a release from the master branch.
```
cp /home/wustl/nrg-svc-jenkins/.m2/repository/org/nrg/xnat/web/1.8.2.2/web-1.8.2.2.jar /tmp/web-1.8.3.jar
~/apps/apache-maven-3.6.3/bin/mvn install:install-file -DgroupId=org.nrg.xnat -DartifactId=web -Dpackaging=jar -Dversion=1.8.3 -Dfile=/tmp/web-1.8.3.jar

* On the CI system, start the build at 305_DICOM_Session_Builders
* Assuming that goes all the way to completion, start the entire build again at 100_Parent
```

## 1.8.4-RC

### Create RC branch from develop
```
Create 1.8.4-rc folder to hold scripts and inputs
* mkdir 1.8.4-rc
* cp repolist-https.json 1.8.4-rc/repolist_a.json
* Remove entries for items that do not use the XNAT release numbers.
** DICOM ExtAttr
** Mizer
** DICOM Edit 4
** DICOM Edit 6
** DICOM Edit Pixels
** DICOM Ecat
* time ./1.8.4-rc/task_a.sh
** This creates the 1.8.4-RC-SNAPSHOT branches and pushes those to bitbucket / github

On the CI system, in the folder: /var/lib/jenkins/jobs/XNAT_Release_Candidate/jobs
 find . -name config.xml -exec sed -i -e 's/1.8.3-rc/1.8.4-rc/' {} \;
 stop/restart Jenkins
 kick off the Release Candidate build by starting Parent
```

### Using a prior version of web.jar so we can build xnat-data-models
These next steps are on the CI system.
```
cp /home/wustl/nrg-svc-jenkins/.m2/repository/org/nrg/xnat/web/1.8.4-SNAPSHOT/web-1.8.4-SNAPSHOT.jar /tmp/web-1.8.4-RC-SNAPSHOT.jar
~/apps/apache-maven-3.6.3/bin/mvn install:install-file -DgroupId=org.nrg.xnat -DartifactId=web -Dpackaging=jar -Dversion=1.8.4-RC-SNAPSHOT  -Dfile=/tmp/web-1.8.4-RC-SNAPSHOT.jar
cd /var/lib/jenkins/jobs/XNAT_Release_Build/jobs/280_Data_Models/workspace
./gradlew clean jar publishMavenJavaPublicationToMavenLocal
```
You will find several different task_?.sh files and repository_?.json files in the folder 1.8.4-rc because we ran into some issues with building on the CI system. The next update (1.8.5-SNAPSHOT) is going to just push the changes to the various code repositories without a local build so we can manage the build on the CI system.

## 1.8.5.SNAPSHOT
On ci.xnat.org / ci-internal.nrg.wustl.edu
```
sudo su - jenkins
cp /home/wustl/nrg-svc-jenkins/.m2/repository/org/nrg/xnat/web/1.8.4-SNAPSHOT/web-1.8.4-SNAPSHOT.jar /tmp/web-1.8.4-RC-SNAPSHOT.jar
~/apps/apache-maven-3.6.3/bin/mvn install:install-file -DgroupId=org.nrg.xnat -DartifactId=web -Dpackaging=jar -Dversion=1.8.4-RC-SNAPSHOT  -Dfile=/tmp/web-1.8.4-RC-SNAPSHOT.jar
cd /var/lib/jenkins/jobs/XNAT_Release_Build/jobs/280_Data_Models/workspace
./gradlew clean jar publishMavenJavaPublicationToMavenLocal
```
You will find several different task_?.sh files and repository_?.json files in the folder 1.8.4-rc because we ran into some issues with building on the CI system. The next update (1.8.5-SNAPSHOT) is going to just push the changes to the various code repositories without a local build so we can manage the build on the CI system.
