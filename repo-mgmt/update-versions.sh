#!/bin/bash

set -e

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

usage() {
cat<<EOF
    USAGE: $0 -w [working directory] -j [json repository list config] -b [source branch]
    OPTIONAL: 
        -u (no arg, indicates you will not update version)
        -l (no arg, indicates you will not perform a local build)
        -x (no arg, indicates you will not push commits to code repositories)
        -p (no arg, indicates you will not publish)
        -c (no arg, indicates you want to resolve merge conflicts with incoming branch's changes
        -i (no arg, indicates you will interactively fix a failing build)
        -n NEW_BRANCH (use if you want to create a new branch with updated versions)
        -d DEST_BRANCH (use if you want to merge your SOURCE_BRANCH into an existing branch)
        -y DEPENDENCY_JSON (build these repos locally prior to running the version changes)
        -r RESUME (resume from this alias - script will raise for issues, this lets you resume where you left off)

json repository list config file must contain a list of the repositories you wish to update in the format below. 

Required keys:
    "repo":"ssh://git@bitbucket.org/xnatdev/test"   # repo 
    "alias":"test"                                  # alias name for convenience
    "cmd":"mvn clean install"                       # build command

Optional keys:
    "oldVersion":"1.8.5-SNAPSHOT"                   # current version
    "newVersion":"1.8.6-SNAPSHOT"                   # version to which we'll update
    "xnatOldVersion":"1.8.5-SNAPSHOT"               # current version of XNAT for repos/plugins with separate versioning
    "xnatNewVersion:"1.8.6-SNAPSHOT"                # updated version of XNAT for repos/plugins with separate versioning
    "depVersions:[["cs-1.2.3:fat","cs-1.2.4:fat"]]  # list of "tuples" containing dependency version updates [[target, replace], [other, replace]]
    "branch":"client"                               # additional branch to update (update will occur on this branch, not on DEST_BRANCH)
    "separateVersion":true                          # if the repo tracks its own version and parent should be updated accordingly
    "updateChildren":true                           # a few repos (ecat, dicom-xnat) have children that also need updating

Note that parent MUST have alias "parent", data-models must have alias "xnat-data-models", and container service must have alias "container-service"
               
EOF
}


echo "Setting options:"
while getopts "w:j:b:n:d:y:r:ulxpci" opt; do
    case ${opt} in
        w)  echo working dir $OPTARG
            XNAT_DIR=$OPTARG
            ;;
        j)  echo list json $OPTARG
            LIST_JSON=$OPTARG
            ;;
        b)  echo source branch $OPTARG
            SOURCE_BRANCH=$OPTARG
            ;;
        n)  echo new branch $OPTARG
            NEW_BRANCH=$OPTARG
            ;;
        d)  echo dest branch $OPTARG
            DEST_BRANCH=$OPTARG
            ;;
        y)  echo dependency json $OPTARG
            DEPENDENCY_JSON=$OPTARG
            ;;
        r)  echo resume at $OPTARG
            RESUME=$OPTARG
            ;;
        u)  echo disable update version
            NO_UPDATE_FLAG="--noUpdate"
            ;;
        l)  echo disable local build
            NO_BUILD_FLAG="--noBuild"
            ;;
        x)  echo disable push to code repositories
            NO_PUSH_FLAG="--noPush"
            ;;
        p)  echo disable publish
            NO_PUB_FLAG="--noPublish"
            ;;
        c)  echo merge conflicts use theirs
            CONFLICT_FLAG="--conflictTheirs"
            ;;
        i)  echo interactively review failures
            INT_FLAG="--interactive"
            ;;
        \?) usage
            exit 1
            ;;
        *)  usage
            exit 1
            ;;

    esac
done

if [[ -z $XNAT_DIR || -z $LIST_JSON || -z $SOURCE_BRANCH ]]; then
    usage
    exit 1
fi

if [[ ! -e $LIST_JSON ]]; then
    echo "$LIST_JSON does not exist"
    exit 1  
fi  

export STAGING_DIR="$XNAT_DIR/staging"
export BUILD_DIR="$XNAT_DIR/build" 

mkdir -p ${STAGING_DIR}
cd ${STAGING_DIR}

echo "Running updates"
# install dependencies
if [[ -n $DEPENDENCY_JSON ]]; then
    if [[ ! -e $DEPENDENCY_JSON ]]; then
        echo "$DEPENDENCY_JSON does not exist"
        exit 1
    fi

    rm -rf ${BUILD_DIR}
    mkdir -p ${BUILD_DIR}
    python3 $SCRIPTS_DIR/pullRepos.py --config $DEPENDENCY_JSON --verFile $STAGING_DIR/ignore.txt
    python3 $SCRIPTS_DIR/buildCode.py --config $DEPENDENCY_JSON --staging ${STAGING_DIR} --build ${BUILD_DIR}
fi
rm -rf ./docker
cp -r $SCRIPTS_DIR/docker ./

python3 $SCRIPTS_DIR/updateVersion.py $NO_UPDATE_FLAG $NO_BUILD_FLAG $NO_PUSH_FLAG $NO_PUB_FLAG $CONFLICT_FLAG $INT_FLAG \
    --config $LIST_JSON ${RESUME:+ --resume $RESUME} \
    --sourceBranch $SOURCE_BRANCH ${DEST_BRANCH:+ --destBranch $DEST_BRANCH} ${NEW_BRANCH:+ --newBranch $NEW_BRANCH}
